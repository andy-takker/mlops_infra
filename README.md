# Infrastructure for MLOps

## Services

```plantuml
left to right direction
@startuml
actor User as u
package "MLFlow Group" as mlgroup {
    component "MLFlow" as mlflow
    component "Nginx" <<Proxy>> as nginx
    database "PostgreSQL" <<RDBMS>> as db
    storage "MinIO" <<S3>> as s3

    mlflow <--> s3
    mlflow <--> db
    nginx <-> mlflow
    u <--> nginx
}

package "Metrics Group" as grafana_group {
    component "Grafana" as grafana
    component "Prometheus" as prom

    u <--> grafana

    grafana <--> prom
}

@enduml
```

MLFlow services:

- `MLFlow` - registry for working with trained models
- `MinIO` - S3 storage for MLFlow
- `PosgreSQL` - database for MLFlow
- `Nginx` - proxy server for basic auth for MLFlow

Monitoring services:

- `Grafana` - manager of dashboards
- `Prometheus` - metric manager

## Environment Variables

Template with whole list of envs is in `.env.dev`.

- `POSTGRES_USER` - username for database connection
- `POSTGRES_PASSWORD` - passwor for database connection
- `POSTGRES_DB` - db name for mlflow
- `POSTGRES_HOST` - hostname or IP-address for database connection
- `POSTGRES_PORT` - port for database connection (default, 5432)
- `MLFLOW_PORT` - inner port for mlflow (using for upstream in nginx)
- `MLFLOW_USER` - username for auth in mlflow by basic authorization (nginx provided)
- `MLFLOW_PASSWORD` - password for auth in mlflow by basic authorization (nginx provided)
- `AWS_ACCESS_KEY_ID` - access key for connection mlflow with minio
- `AWS_SECRET_ACCESS_KEY` - secret access key for connection mlflow with minio